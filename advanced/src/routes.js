import Search from '@/components/Search.vue'
import About from '@/components/About.vue'
import TrackDetails from '@/components/TrackDetail.vue'
const routes = [
  { path : '/', component: Search, name: 'search' },
  { path : '/track/:id', component: TrackDetails, name: 'track' },
  { path : '/about', component: About, name: 'about' }
]

export default routes
